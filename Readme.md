# Introduction
Remote operating system (OS)  image deployment refers to deploying  the image stored on a remote server on to  bare metal client  computer having local hard disk (hdd) with no or little software initially on hard disk ,  connected to a LAN  network . The image here refers to an “disk image” i.e.  containing operating system (OS)  with its root filesystem and zero or more other partitions of  disk.
A similar project to this  is  [Fog Project](https://fogproject.org/), but it doesn't cache and always downloads the image from server, and not so suitable for frequent image deployment as it would cause high network traffic.

# GOAL
What we propose to do is to develop remote OS image deployment architecture wherein the client side users  have choice to  deploy any of the images  on every reboot (i.e. very frequent imaging) on PXE supported computers and with cache mechanism to avoid re-downloading of the  image .<br>

# Objectives
*  Develop Client oriented OS Image deployment i.e. the client has ability to choose the required image to be deployed.
*  Develop cache mechanism to prevent re-downloading of images from the server.
*  Automate the above stated deployment process along with ability to boot automatically the client with OS deployed image after deployment.
*  Always give client users a choice  of the image to be deployed on every reboot of the computer i.e. on every reboot a image (same or different ) is deployed.

  
# Basic understanding

For any of the remote image deployment software following are basic elements as depicted in fig 1 ![alt text](basic system architecture.png): <br>
1) DHCP server -  assigns ip address to LAN PXE client, give name of  boot file and IP address of tftp server to access it from <br>
2) TFTP server  -   stores boot files which   PXE client downloads after ip assignment <br>
3) Remote  server - the remote server which stores images and used to transfer files from server to client . We have used NFS server as remote server as it has higher speed file  transfer. <br>
4) Network Bootloader and tool for managing the deployment - A Network bootloader to be able to boot a tiny network OS / itself   manage the deployment,  and if needed tiny OS being loaded from TFTP server. We wanted to extend the grub bootloader as both network bootloader and tool which manages deployment - to be able to do imaging and cache , so that after deploying image one can straight away boot from the hdd without the need of a reboot. <br>
<br>
GRUB seems insufficient in following ways:<br>
  i) Grub doesn’t provide much support for imaging although GRUB4DOS - a project based on GNU GRUB implements dd for imaging from local disk and not nfs server - but does not provide any  tool / any way of  customising the imaging tool for fast imaging, <br>
 ii) no NFS mount support to transfer files i.e. images <br>
 iii) flexible partitioning tool - to create hidden partition in local hdd <br>
And to develop modules for these above stated insufficiency seemed little infeasible in terms of time to develop and also with its little support in built functions to above things and need to keep the size of grub executable to minimum, led us to opt for a different approach. <br>
Different approach being to network boot using grub and boot to  tiny os - which does the management of imaging and reboots after deployment to deployed image OS. <br>

# Setup
 Each main directory in this repository has Readme file describing that particular directory. Please also go through them when setting up this system.
 Setting up the OS image deployment system mainly involves setting up of three servers - DHCP, TFTP, NFS, whose configuration and needed files are present as respective sub-directories of server-config directory.<br>
 Tools/ source to build grub and tiny OS is present in directories grub_build and buildroot-2019-05 respectively.<br>
 
 Remote OS image depoloyment can be setup in  following two ways: <br>
    
1) LAN connected client machines : 
        In this setup client machines are connected to servers in LAN.
        
        DHCP server setup
        It can be integrated with existing DHCP server by changing the exisiting DHCP configuration file to support PXE boot. 
        See here to change the dhcp configuration.
        For new setup on a machine, install isc-dhcp-server and edit interface option in /etc/default/isc-dhcp-server
        to listen to LAN network interface such as "eth0","enp0s2".
        Edit /etc/dhcp/dhcpd.conf for desired ip range address assigning and to edit other option as required. 
Please refer [here](https://githepia.hesge.ch/abhilash.venkates/remote-imaging/tree/master/server-config/dhcp#dhcp-configuration-files) for more information to edit dhcpd.conf file.
        
        TFTP server setup
        Install tftpd-hpa,tftp packages on the target machine, copy tftpd-hpa configuration file
        from server-config/tftp directory to /etc/default/tftpd-hpa.
        Restart the service and voila tftp is running.
        Transfer tftpboot directory from server-config/tftp to /, so that tftpboot absolute address is "/tftpboot". 
        Note: By default tftpboot directory contains grub image and modules for bios i386-pc platform. 
        If you want grub of differnt configuration, you need to build grub source prsent at grub_build. 
Please  refer [here](https://githepia.hesge.ch/abhilash.venkates/remote-imaging/tree/master/grub_build#building-network-bootable-grub) to build grub.
        
        NFS server setup 
        Install nfs-kernel-server package and start the service.
        Copy nfsroot directory from  server-config/nfs directory to /, so that absolute adress becomes /nfsroot.
        /nfsroot is the directory which will be shared to client as nfs-share. 
        Copy the exports file from server-config/nfs to /etc/exports and start the nfs service.
Note: exports by default shares /nfsroot to all clients without restriction. If you want to restrict the access to a network, refer [here](https://githepia.hesge.ch/abhilash.venkates/remote-imaging/tree/master/server-config/nfs#configuration-file).
The tiny OS, by default mounts NFS server having ip address 192.168.100, to change mount point refer [here](https://githepia.hesge.ch/abhilash.venkates/remote-imaging/tree/master/buildroot-2019.05#editing-the-nfs-server-mount).
        
        
        
        Client Machine 
        Set the network boot option of the clients as the first in the boot sequence. 
        After setup of all above servers and running, boot the client 
        and the client boots to grub menu as below : 
    
2) Simulation using VM cients

        Create host only adapter in VM having ip address as 192.168.56.100.
        VM clients network adapter  needs to be configured to host only adapter.
        Then setup all servers on host machine with default configuration.
        Make netwook boot option as the first in boot sequence of VM clients.
        
        
        
        
        
        
 
 
 

