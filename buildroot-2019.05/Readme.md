# What is Buildroot?
Buildroot is a simple, efficient and easy-to-use tool to generate embedded
Linux systems through cross-compilation.

The documentation can be found in docs/manual. You can generate a text
document with 'make manual-text' and read output/docs/manual/manual.text.
Online documentation can be found at http://buildroot.org/docs.html <br>

This tool was used to build a customised linux OS called "tiny OS", which has 
minimum set of packages required for image deployment, just like an ubuntu installer.
The main functionalities of this "tiny OS" is to 
* Create partition ( required for creating hidden partition).
* Display the available images on remote server (NFS server) and allow client users to choose.
* Download the image and cache the image to local hard disk (in hidden partition).
* Deploy the image onto the hard disk and reboot to deployed image through the help of grub by providing deployed signal to grub, just before it reboots.
* Automate all the above process.

Note : There are two versions of "tiny OS" namely :
1. auto_os : This version automates the image deployment, which is the needed one.
2. manual_os : This version doesn't automate image deployment, instead tiny OS boots a tty terminal, the user has to manually execute the script of deployment either "/root/deploy_log.sh" or "root/ideploy.sh" for deployment. 
              It is purely for debugging purpose and to understand how deployment works.


# Pre-Requisite Packages to be installed
* which
* sed
* make (version 3.81 or any later)
* binutils
* build-essential (only for Debian based systems)
* gcc (version 4.4 or any later)
* g++ (version 4.4 or any later)
* bash
* patch
* gzip
* bzip2
* perl (version 5.8.7 or any later)
* tar
* cpio
* python (version 2.7 or any later)
* unzip
* rsync
* file (must be in /usr/bin/file)
* bc 
* wget
* ncurses5
* libelf-dev

# Building the tiny OS
 With following steps of building from source, one can reproduce the tiny OS:<br>
 *  Enter in buildroot and enter in terminal ,"make menuconfig" , this gives the menu with options selected that were used to buld tiny OS ,  if you want additional functionally you can edit it or if none just save it before you exit.
 * Enter command "make busybox-menuconfig"  and select load configuration file and enter the abbsolute path of "busybox.config" file present in this directory and save configuration before you exit.
 * Enter the command "make", it takes 30-45mins to build.
 * Copy inittab and fstab file from either manual_os (not automated deployment) /auto_os (automated deployment ) depdending on the type of kernel to "output/taget/etc/" (directory path is relative to buildroot) .
 * Enter command "make" to rebuild grub, it build faster in 2 -3 mins.
 * Copy all files in "output/image" to manual_os or autos_os directory in tftpboot and also to boot directory of nfsroot.

# Editing the  NFS-server mount 
** To change the NFS server mount  from its default "192.168.56.100:/nfsroot" (192.168.56.100 - nfs server ip), tiny OS must be rebuilt, existing tiny Os images in the tftpboot cannot be used.**.

 The mount point of NFS needs to be included in the fstab file, the need might arise due to different ip address of NFS server.To do this follow the below steps: <br>
 *  Make first build of tiny Os as instructed above 
 *  Copy inittab and fstab file from either manual_os (not automated deployment) /auto_os (automated deployment ) depdending on the type of kernel to "output/taget/etc/" (directory path is relative to buildroot) .
 *  Before building for second time, edit the fstab file in "output/target/etc", to include changed mount point in the form "ip:/nfsroot", where ip is the ip of NFS server.
 *  Build the second time and then copy all files in "output/image" to manual_os or autos_os directory in tftpboot and also to boot directory of nfsroot.
 
 
 
 


 

