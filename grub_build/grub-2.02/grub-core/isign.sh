#!bin/sh
gcc -Icommands -I./commands -nostdinc -isystem /usr/lib/gcc/x86_64-linux-gnu/7/include/  \
       -I ../include -I .. -I ../include -Wall -W  -Os  -DGRUB_MACHINE_PCBIOS=1 -Wall -W -Wshadow  \
      -Wpointer-arith -Wmissing-prototypes  -Wundef -Wstrict-prototypes -g -falign-jumps=1 \
      -falign-loops=1 -falign-functions=1 -mno-mmx -mno-sse -mno-sse2 -mno-3dnow \
      -fno-dwarf2-cfi-asm  -fno-stack-protector -mno-stack-arg-probe -Werror -fno-builtin \
      -MD  -c -o isign_mod-commands_isign.o ./commands/isign.c



# rm -f pre-isign.o
# gcc -m32 -nostdlib -m32 -Wl,--build-id=none -Wl,-r,-d -o pre-isign.o isign_mod-commands_isign.o
# nm -g --defined-only -P -p pre-isign.o | sed 's/^\([^ ]*\).*/\1 isign/' > def-isign.lst
# echo 'isign' > und-isign.lst
# nm -u -P -p pre-isign.o | cut -f1 -d' ' >> und-isign.lst

# GRUBCFLAGS = -Os -DGRUB_MACHINE_PCBIOS=1 -falign-jumps=1 -falign-loops=1 -falign-functions=1 \
#     -mno-mmx -mno-sse -mno-sse2 -mno-3dnow  -fno-stack-protector -mno-stack-arg-probe \
#     -mrtd -mregparm=3 -DGRUB_UTIL -ffreestanding -DGRUB_FILE=__FILE__