#include <grub/dl.h>
#include <grub/disk.h>
#include <grub/misc.h>
#include <grub/extcmd.h>


#define GRUB_ISIGN_MATCH 101
#define GRUB_ISIGN_MISMATCH 100
#define GRUB_ISIGN_WRITE 102

GRUB_MOD_LICENSE ("GPLv3+");

static const struct grub_arg_option options[] = {
  {"check", 'c', 0, N_("Check the specified (string) signature with that present at the end of disk"), 0,
   ARG_TYPE_STRING},
  {"write", 'w', 0, N_("Write the signature (string)  at the end of the disk "), 0, ARG_TYPE_STRING},
   {0, 0, 0, 0, 0, 0}
};

static grub_err_t
grub_cmd_isign(grub_extcmd_context_t ctxt,int argc, char **args) 
{ grub_err_t err=0;
  struct grub_arg_list *state = ctxt->state;
  char buf[GRUB_DISK_SECTOR_SIZE * 4];
  grub_ssize_t  length;
  grub_disk_addr_t skip;
  int namelen;
  char *str=0;  //pointer to specified string 

  if (argc != 1)
    return grub_error (GRUB_ERR_BAD_ARGUMENT, N_("disk file expected"));
   namelen = grub_strlen (args[0]);

   if (state[0].set)
   { 
     if(!state[0].arg)
       return grub_error (GRUB_ERR_BAD_ARGUMENT,
         "enter string");
      str=state[0].arg;
   }

   if (state[1].set)
   { 
     if(!state[1].arg)
       return grub_error (GRUB_ERR_BAD_ARGUMENT,
         "enter string");
      str=state[1].arg;
   }

  if ((args[0][0] == '(') && (args[0][namelen - 1] == ')'))
    {
      grub_disk_t disk;
      grub_disk_addr_t sector;
      grub_size_t ofs;

      args[0][namelen - 1] = 0;
      disk = grub_disk_open (&args[0][1]);
      if (! disk)
        return 0;

      skip = grub_disk_get_size (disk) >> 1 ;
      skip = skip<<10;
      skip=skip-200;   // start writing from 200 bytes from the end of disk
      //grub_printf(" bytes skipped %llu\n",skip);
      length = grub_strlen(str);

      
      sector = (skip >> (GRUB_DISK_SECTOR_BITS + 2)) * 4;
      ofs = skip & (GRUB_DISK_SECTOR_SIZE * 4 - 1);
      
       if (state[0].set && !state[1].set)                             // checking the signature 
        {  if (grub_disk_read (disk, sector, ofs, length, buf))
            return 0;

          buf[length]='\0';
          //grub_printf("signature reads %s \n length %d\n",(char*) buf,length);
          err=GRUB_ISIGN_MATCH;
          if(grub_strcmp(str,buf))
          {
          
            //grub_printf("Not same sign\n");
            err=GRUB_ISIGN_MISMATCH;

           }

        }

         if(!state[0].set && state[1].set)                      // writing the specified signature (string)
          { grub_disk_write(disk, sector, ofs, length, str);
            err=GRUB_ISIGN_WRITE;
            
            if (grub_disk_read (disk, sector, ofs, length, buf))
            return 0;

            buf[length]='\0';
            //grub_printf("signature  reads %s\n",(char*) buf);
          }


          // hexdump (skip, buf, len);
      grub_disk_close (disk);
    }
    
    else 
      {
        return grub_error (GRUB_ERR_BAD_ARGUMENT,
         "enter disk file in format '(hdx)' x is non-negative integer");
      }
  

  return err;
}

static grub_extcmd_t cmd;
GRUB_MOD_INIT (isign)
{
  cmd = grub_register_extcmd ("isign",grub_cmd_isign, 0,
            N_("[OPTION] hardisk file"),
            N_("Check or write the signature (string)  at the end of disk"),
            options);
}

GRUB_MOD_FINI (isign)
{
  grub_unregister_extcmd (cmd);
}



