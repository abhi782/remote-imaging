# Network bootable grub
Currently this project is built on **grub-2.02**. GRUB is  loaded from TFTP server by the PXE client at the start of the network boot. First 
PXE client loads the grub core image called "core.O" which contains the kernel with necessary modules, and then loads other required modules from TFTP server.
New module called "isign.mod" was added to grub source, it checks/writes the specified string  at the end of harddisk file specified.This is used 
for signaling whether grub as to boot from network tiny OS images or from harddisk. Whenever image is deployed, signature (string) is wriiten at the end of disk, so grub on reboot boots into
harddisk, but just before it boots to harddisk it resets the signal i.e. erases the signature at the end of disk, so that on next reboot it boots to tiny Os and user can choose the OS image from menu.

# Pre-requisite packages to build grub

* gcc >4.1.3
* make
* bison >2.3 
* gettext >0.17
* binutils >2.9.1.0.23
* Flex > 2.5.35
* libdevmapper 1.02.34
* glibc 
* python>2.6
* autoconf >2.60
* automake >1.10.1
 

File packages.txt contains above list of packages needed to build grub.

# Building network bootable grub
Directory grub-2.02 and grub-2.02.tar.gz contains the modified version of grub source code whereas directory grub-2.02_source contains [offical source](ftp://ftp.gnu.org/gnu/grub/) code of grub.
When grub is built, both - 

*  kernal image,modules 

*  grub-utilities like grub-mknetdir, grub-mkconfigure, grub-install are built.

The grub-utilities can be built either dynamically or statically, but it is better to have static build. To get static build, execute static_build.sh. File static_build.sh has below build commands and can be directly used to build default configuration i.e.
grub is installed in "~/local" directory and TFTP bootable grub directory is at '../server-config/tftp/tftpboot', then this tftpboot directory has to be copied/moved to "/tftpboot" directory on TFTP server. <br>

Grub is built using GNU make system, which generally involves following three steps :
1.  configure : Configure options like CFLAGS, cross-compile, directory where grub is installed  etc. This is done by executing ./configure script with options .
2.  make : Tool that  builds the grub from Makefile, it compiles and links different dependent files/libraries.
3.  make install : This installs the package in the place specified with configure.

To create core image of grub, previously built utility grub-mkimage  is used with following options :
*  directory : directory where grub modules are present.
*  Name of modules to be included in the core image : Here tftp, pxe, is included to allow download of further modules/ tiny OS image  from TFTP server,
  biosdisk, part_msdos, ext2 modules are included to give grub ability to recognise bios disk, MsDos label partition table and filesystem respectively.
*  O : The output platform format where this grub image can run on, here "i386-pc-pxe" format is used, for more architecture format, run grub-mkimage --help.
* output : The place where output  of the image needs to be stored. 

To create TFTP bootable grub directory, previously built utility grub-mknetdir is used which basically transfers files from built modules to TFTP server shared directory in specific format, with follwoing  options :
*  net-directory : path to TFTP server share directory.
*  subdir : sub-directory inside TFTP server share directory where grub should be installed
*  d : directory where grub modules are present

Finally the "core.0" image built by grub-mkimage is moved/copied to TFTP server share directory having grub modules. <br>
The DHCP server bootfilename location  should point to "core.0" image file path relative to TFTP directory (i.e. here "/tftpboot", hence its boot/grub/i386-pc/core.0).<br><br>
Note: 
1.  To understand more on grub and building grub , refer [here](http://www.dolda2000.com/~fredrik/doc/grub2).
2.  To build network bootable grub for different platforms/firmware like UEFI etc, please refer end of this [article](http://lukeluo.blogspot.com/2013/06/grub-how-to6-pxe-boot.html).
3.  Grub 2.02 [offical source](ftp://ftp.gnu.org/gnu/grub/) used for this project.
4.  Please refer this frequent  [issue](https://gitedu.hesge.ch/abhilash.venkates/remote-imaging/issues/1#note_7961) if you face problem in building grub.



