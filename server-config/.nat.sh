virtual_box_interface="vboxnet0"
ip_addr="192.168.56.0/24"
host_interface="enp0s25"
sudo iptables -A FORWARD -o $host_interface -i $virtual_box_interface -s $ip_addr-m conntrack --ctstate NEW -j ACCEPT

sudo iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT


 sudo iptables -t nat -F POSTROUTING

sudo iptables -t nat -A POSTROUTING -o $host_interface  -j MASQUERADE

 sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"

service isc-dhcp-server restart

