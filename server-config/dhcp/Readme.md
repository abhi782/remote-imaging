# DHCP Configuration files
 For configuration of DHCP, following files are needed : <br>
 *  isc-dhcp-server :<br>
    It is used to configure the interface through which DHCP server listens and serves the DHCP requests. To change the default interface configuration present here i.e. "vboxnet0",
    change the INTERFACESv4 or INTERFACESv6 ( for IPv6) to the desired interface such as "eth0", "enp0s2". Location of file is /etc/default/isc-dhcp-server.


 * dhcpd.conf : <br>
    It is used to configure the range of ip address ,subnet,netmask, and other options, to assign the clients. During PXE boot of client, PXE client requests DHCP server for ip address,
    bootfile-name and tftp server name /ip, so that client knows where to download the grub bootloader and which location, but a normal client DHCP request ( i.e. DHCP request by the kernel at startup procces)
    only needs ip address,sunet,netmask and no tftp server name or bootfile name. 
    To differniate, this a client can be grouped to PXEclients or  normal clients based on on DHCP option called vendor-class-identifier and match that option to "PXEClient" PXEclients.
    option tftpserver-name - give the static ip address of tftp server in string format
    option boot-filename - give the location of core.0 ( core image of grub ) relative to tftp directory shared. Here, on TFTP server "/tftpboot" is shared, and core.0 is present at "/tftpboot/boot/grub/i386-pc/core.0" .
    So, the string "/boot/grub/i386-pc/core.0" is assigned.
    option domain-name-servers - ip address of DNS server
    option routers - ip address of router
    Location of dhcpd.conf file is "/etc/dhcp/dhcpd.conf". 
    It can be integrated with existing dhcp server by editing this file and matching PXEclients based on their vendor-class-identifier option.

    NOTE: vendor-class-identifer="PXEClient"matches all PXEclient computers,irrespective of firrmware. As BIOS and UEFI firmwware requires different grub files, it is important that
    we get to know which firmware client it is and supply appropriate platform files, by default the configuration present here assume omly BIOS firmware clients.
    Please refer [this](http://lukeluo.blogspot.com/2013/06/grub-how-to6-pxe-boot.html), at the end of the site, it provides some ideas, to match UEFI clients.
    Please refer [this](https://kb.isc.org/docs/aa-00333) for more information on dhcp configuration and dhcp options

*    PLease refer  [BIOS and UEFI co-existence](https://wiki.fogproject.org/wiki/index.php/BIOS_and_UEFI_Co-Existence)



    