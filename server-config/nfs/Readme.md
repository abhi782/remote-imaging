# Configuration file

nfs-kernel-server package needs to be installed. Configuration file exports located at "/etc/exports" is used to export directory(ies) as NFS-share, which clients can mount.<br>
It should include folllowing details : <br>
*  The path of directory shared needs to be specified, here "/nfsroot" is shared.
*  network/list of ip adresses of computers where it can be mounted, here anyone can mount it i.e. "*", to restrict to network specify something like ip addr/subnet mask.Eg: "192.168.56.100/255.255.255.0".  
*  options of NFS server - 
    *  rw - Allow both read and write requests on this NFS volume
    * async -This option allows the NFS server to violate the NFS protocol and reply to requests before any changes made by that request have been committed to stable storage (e.g. disc drive).
            Using this option usually improves performance, but at the cost that an unclean server restart (i.e. a crash) can cause data to be lost or corrupted. 
    *  no_subtree_check - This option disables subtree checking, which has mild security implications.
    *  fsid - NFS needs to be able to identify each filesystem that it exports.For NFSv4, there is a distinguished filesystem which is the root of all exported filesystem. This is specified with fsid=root or fsid=0 both of which mean exactly the same thing. 
    *  no_root_squash - Turn off root squashing. This option is mainly useful for diskless clients. 
    *  insecure - The insecure option in this entry also allows clients with NFS implementations that don't use a reserved port for NFS. 

Please refer [this](https://linux.die.net/man/5/exports) for more information on options.
               

# What does NFS server contain 
"nfsroot" directory here gives example model of things to be in NFS server which can be  copied to nfs server at / and its directory structure is summarised in in the file directory_structure.
It contains following folders:<br>
*  boot : It contains tiny OS images for manual imaging i.e. in manual_os sub-directory and automatioc _imaging i.e. in auto_os. It should neccessarily contain mbr folder, where tiny Os saves the MBR of client 
         which has the information of the hidden partition as MAC-address.img file.
* images : In this folder the images to be deployed is stored. Format of storing the image is as follows:
        
        *  Image needs to be captured by dd command  and needs to be compressed using gzip to  "name.img.gz" file
        *  "name".md5 stores the md5sum of compressed file "name.img.gz".
        *  "name".partition is fdisk -l output of disk that was imaged
        *  "name".size is size of the compressed file "name.img.size" in KB  ( basically obtained by du command)
        *  put all above files in directory called "name" inside images.
        *  This above is done by the script .script.sh , just have to place the "name.img.gz", in "name" directory  and modify a bit of .script.sh
        
*  log : The tiny Os stores the log output(MAC.log)  of  the image deployment and cpu details (MAC.info) of each client in seperate subdirectories based on MAC address.

*  *.sh files : Collection of scripts which perform deployment of image from NFS server.
        
        * ideploy.sh - It is the main Image deployment script, it defines variables, executes  partition.sh, menu.sh, deploying.sh and finally put signature at the end of the disk to signal image deployment.
        * partition.sh - to check, or/and create , and mount hidden partition
        * menu.sh - GUI to display and selection of avilable images on NFS-server
        * deploying.sh - Deploys the selected image
        * deploy_log.sh - It calls ideploy.sh and logs its output onto NFS server.
        
*  usr : contains the static build of partimage ( has some errors,not working yet ).


