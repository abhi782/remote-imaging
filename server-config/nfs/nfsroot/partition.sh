#!/bin/bash


#partition.sh 
#Checks for hidden partition and if not found creates one with size equal to 20% of the  total size of disk with fs -ext4
#Then mounts the partition.


if [[ -e $mounting_point_remote/boot/mbr/$MAC.img  ]] ; then
	dd if=$mounting_point_remote/boot/mbr/$MAC.img bs=1 count=512 of=/dev/sda #flushing the hidden partition table from the NFS server onto MBR
	partprobe /dev/sda  && 	echo "hidden partition available"  

else

	dd if=/dev/zero count=512 of=/dev/sda  
	parted -s -a opt /dev/sda mklabel msdos mkpart primary  ext4 $start"B" $end"B" && echo "partition created"  #creating the hidden partition
	partprobe /dev/sda  
	dd if=/dev/sda  count=512 of=$mounting_point_remote/boot/mbr/$MAC.img  
	mke2fs -t ext4 /dev/sda1  && echo "hidden partition created and available"  #formating into ext4 fs
	
fi

# UUID=`blkid | grep  "UUID[^ ]*"| cut -d '"' -f 2` 
mount /dev/sda1 $mounting_point_hidden && echo "mounted"  #mounting the hidden partition