#!/bin/bash


#deploy_log.sh 
#It logs the Image  deployment on  the nfs-server in the "log/MAC-address of pc" folder as MAC.log file, 
#mainly for debugging.
#It also logs cpu details - (processor,memory,disk) of client being deployed in MAC.info file in the same above folder.
#It calls the Image deployment script - ideploy.sh 

clear
umask -S 0000 &> /dev/null
Interface="eth0"
mounting_point_remote="/root"
MAC=`cat /sys/class/net/$Interface/address | tr ":" "-"`
log_dir="$mounting_point_remote/log/$MAC"
log_file="$log_dir/$MAC.log"
cpu_details="$log_dir/$MAC.info"

if [ ! -d  $log_dir ] ;then
 mkdir -p $log_dir
fi

if [ ! -f $cpu_details ] ; then
	echo -e "\n ==========================proccessor===============================\n" >> $cpu_details
	cat /proc/cpuinfo  >> $cpu_details
	echo -e "\n ==========================memory===============================\n" >>  $cpu_details
	cat /proc/meminfo >> $cpu_details
    echo -e "\n ==========================Disks===============================\n" >>  $cpu_details
    parted -l >> $cpu_details
fi

. ideploy.sh &>> $log_file