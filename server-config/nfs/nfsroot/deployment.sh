


function check_md5sum () {
	
	for i in `ls $1/*.md5` ; do
		local_hash=`head -n 1 $i | cut -d " " -f 1` 
		if [[ $local_hash == $hash ]]; then
			return 1
		fi
	done
	return -1

}


size_image=`cat $mounting_point_remote/images/${images[$CHOICE]}/${images[$CHOICE]}.partition | grep "Disk " | head -n 1 | cut -d ',' -f 2 | cut -d ' ' -f 2` && echo "total_size of image=$size_image B"

if [[ $size_image -gt $start  ]]; then
	echo "Insufficient disk space for imaging, need size $size_image B but has $start B" 
	umount $mounting_point_hidden
	DATE=`date`
	boundary="######################################$DATE##############################################"
	echo  -e "\n$boundary\n" 
	exit 0

fi && echo "sufficient space for imaging" 


hash=`head -n 1 $mounting_point_remote/images/${images[$CHOICE]}/${images[$CHOICE]}.md5 | cut -d " " -f 1` 
check_md5sum  "$mounting_point_hidden" 
result=$?
((cache_flag=0))
if [[  $result -ne 1  ]]; then
	
	   echo "not cached, so caching "  
	   size_cache_available=`df $mounting_point_hidden/ | awk '{print $4}' | tail -n 1 ` && echo "size cache available=$size_cache_available KB"   #in KB
	   ((size_cache_available=size_cache_available -4))
	   size_cache_img=`cat $mounting_point_remote/images/${images[$CHOICE]}/${images[$CHOICE]}.size |head -n 1 | awk '{print $1}'` && echo "size of image to download=$size_cache_img KB"    #in KB
	   
	   if [[ $size_cache_img -lt $size_cache_available ]]; then
	   ((cache_flag=1))
	   start_time=`date +%s` && \
	   cp $mounting_point_remote/images/${images[$CHOICE]}/${images[$CHOICE]}.md5  $mounting_point_hidden/ && \
	   cat  $mounting_point_remote/images/${images[$CHOICE]}/${images[$CHOICE]}.img.gz | tee $mounting_point_hidden/${images[$CHOICE]}.img.gz | gunzip -c |  dd bs=4096 of=/dev/sda  && \
 	   end_time=`date +%s` && \
       ((cache_time=end_time - start_time)) && \
       echo "Caching and deployment  of $size_cache_img KB done in $cache_time s "	 

	    else
	    	echo  "not enough space for caching"  #further scope for some cache replacement strategy
	    	((cache_flag=-1))
	    fi

fi  && echo "md5sum checked" 

# dd if=/dev/zero count=2048 of=/dev/sda  
start_time=`date +%s`

 if [[ $cache_flag == 0 ]] ; then

  gunzip -c $mounting_point_hidden/${images[$CHOICE]}.img.gz    | dd bs=128k  of=/dev/sda  && echo "deployment of ${images[$CHOICE]} done yeah " 

 elif [[ $cache_flag == -1 ]]; then

 cat $mounting_point_remote/images/${images[$CHOICE]}/${images[$CHOICE]}.img.gz |gunzip -c | dd  of=/dev/sda  bs=4096 && echo "deployment of ${images[$CHOICE]} done" 

fi

end_time=`date +%s`
((deploy_time= end_time - start_time))
umount $mounting_point_hidden && echo "time to deploy $size_image is $deploy_time s"  
sleep 3