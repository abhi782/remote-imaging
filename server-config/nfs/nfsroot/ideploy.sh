#!/bin/bash


#ideploy.sh 
#Image deployment script, defines variables. calls other utility functional scripts needed for deployment which are :
#partition.sh -- to check, or/and create , and mount hidden partition
#menu.sh - GUI to display and selection of avilable images on NFS-server
#deploying.sh - Deploys the selected image
#Finally, this script writes signature at the end of disk ( starting from 200 bytes from the end ) to signal 
#network grub to boot from local hdd rather than from the tiny operating system image present on tftp server


mounting_point_remote="/root" # mounting point of NFS server in tiny operating system 
mounting_point_hidden="/mnt"  # mount point of hidden partition 
DATE=`date`
Interface="eth0" 
MAC=`cat /sys/class/net/$Interface/address`  
log_file="$mounting_point_remote/log/$MAC.log" #log_file address on NFS-server
signature="hepia2015"       #Signature as signal  which is written to end of disk  after image deployment
images_dir="$mounting_point_remote/images"
boundary="######################################$DATE##############################################"
echo  -e "\n$boundary\n" 

((size_num= `fdisk -l | grep "Disk /dev/sda" | cut -d ',' -f 2 | cut -d ' ' -f 2`  )) && echo "Total size of disk=$size_num bytes"   # size in bytes
((size_part= 20 * size_num / 100)) && echo "size of hidden partition=$size_part bytes" 
((start=size_num - size_part - 4096)) && echo "size of disk for imaging=$start bytes"	
((end=start + size_part )) && echo "End byte=$end bytes" 




. $mounting_point_remote/partition.sh
. $mounting_point_remote/menu.sh
. $mounting_point_remote/deploying.sh



((size_num_sign=size_num - 200)) #Starting location of signature 

echo "location of signature at  $size_num_sign B" && echo " signature is $signature"
echo -ne "$signature" | dd of=/dev/sda seek=$size_num_sign bs=1 iflag=skip_bytes  

 
#clear
echo "rebooting.." 
DATE=`date`
boundary="######################################$DATE##############################################"
echo  -e "\n$boundary\n" 
 cd /
sleep 5 
#reboot