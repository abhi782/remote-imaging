#!/bin/bash


#menu.sh
#Scans the images folder of nfs-server and displays the available images in GUI.
#Dialog package  is used for GUI.

##########################################################################################################
sleep 2

newline='
'

count=0
declare -A IMAGES

cd $mounting_point_remote/
for i in `ls $images_dir` ; do

 options=("${options[@]}"   "$((++count))" "$i" )
  IMAGES[$count]="$i"


done

 HEIGHT=20
 WIDTH=76
 CHOICE_HEIGHT=16


TERMINAL=$(tty)
CHOICE=$(dialog --clear \
                 --title "MENU BOX" \
                 --menu "Select options" \
                 $HEIGHT $WIDTH $CHOICE_HEIGHT \
                 "${options[@]}" \
                2>&1 >$TERMINAL )

echo "chosen image is ${IMAGES[$CHOICE]} " 
#############################################################################################################

