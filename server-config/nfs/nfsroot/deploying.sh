#!/bin/bash


#deploying.sh 
#Checks for cache i.e. in the hidden partition of harddisk, for  selected image if present, and deploys from hidden partition.
#checking is done by comparing md5sum of files present localy and on remote nfs server.
# If image cache not present, downloads the image from nfs server onto hidden partition if space is available, and then deploys the image.
#If no space available in hidden partition, it just deploys from nfs server without making a copy locally.
#Some Cache replacement strategies need to be applied to overcome no-space for cache available problem 


#function to check if the md5sum of the specified image in remote and those present lcoally match

function check_md5sum () {
	
	for i in `ls $1/*.md5` ; do
		local_hash=`head -n 1 $i | cut -d " " -f 1` 
		if [[ $local_hash == $hash ]]; then
			return 1
		fi
	done
	return -1

}

((ALREADY_CACHED=0))
((CACHED_NOW=1))
((NOT_CACHED=-1))
((cache_flag=ALREADY_CACHED)) 
size_image=`cat $images_dir/${IMAGES[$CHOICE]}/${IMAGES[$CHOICE]}.partition | grep "Disk " | head -n 1 | cut -d ',' -f 2 | cut -d ' ' -f 2` && echo "total_size of image=$size_image bytes"

#check if the image to be deployed size is  less than the size of the disk available (=total_disk - hidden partition) for imaging
if [[ $size_image -gt $start  ]]; then 
	echo "Insufficient disk space for imaging, need size $size_image B but has $start bytes" 
	umount $mounting_point_hidden
	DATE=`date`
	boundary="######################################$DATE##############################################"
	echo  -e "\n$boundary\n" 
	exit 0

fi && echo "sufficient space for imaging" 


hash=`head -n 1 $images_dir/${IMAGES[$CHOICE]}/${IMAGES[$CHOICE]}.md5 | cut -d " " -f 1` 
check_md5sum  "$mounting_point_hidden" 
result=$?



#Check if image to be deployed is cached  
if [[  $result -ne 1  ]]; then
	
	echo "not cached, so caching "  
	size_cache_available=`df $mounting_point_hidden/ | awk '{print $4}' | tail -n 1 ` && echo "size cache available=$size_cache_available KB"   #in KB
	((size_cache_available=size_cache_available -4))
	size_cache_img=`cat $images_dir/${IMAGES[$CHOICE]}/${IMAGES[$CHOICE]}.size |head -n 1 | awk '{print $1}'` && echo "size of image to download=$size_cache_img KB"    #in KB
	   
	   #Check enough space available in hidden partition for caching
	if [[ $size_cache_img -lt $size_cache_available ]]; then
		  

		start_time=`date +%s` && \
		cp $images_dir/${IMAGES[$CHOICE]}/${IMAGES[$CHOICE]}.md5  $mounting_point_hidden/ && \
		#Simultaneously make local copy and deploy 
		cat  $images_dir/${IMAGES[$CHOICE]}/${IMAGES[$CHOICE]}.img.gz | tee $mounting_point_hidden/${IMAGES[$CHOICE]}.img.gz | gunzip -c |  dd bs=128k of=/dev/sda  && \
	 	end_time=`date +%s` && \
	    ((cache_time=end_time - start_time)) && \
	    echo "Caching and deployment  of $size_cache_img KB done in $cache_time s "	 && \
	    ((cache_flag=CACHED_NOW))

	else
		echo  "not enough space for caching"  #further scope for some cache replacement strategy
	    	((cache_flag=NOT_CACHED))
	fi

fi  && echo "md5sum checked" 

start_time=`date +%s`
if [[ $cache_flag == $ALREADY_CACHED ]] ; then #deploy from hidden partition 
	gunzip -c $mounting_point_hidden/${IMAGES[$CHOICE]}.img.gz    | dd bs=128k  of=/dev/sda  && echo "deployment of ${IMAGES[$CHOICE]} done yeah " 
elif [[ $cache_flag == $NOT_CACHED ]]; then #deploy from NFS server without making local copy
 	cat $images_dir/${IMAGES[$CHOICE]}/${IMAGES[$CHOICE]}.img.gz |gunzip -c | dd  of=/dev/sda  bs=128k && echo "deployment of ${IMAGES[$CHOICE]} done" 
fi
end_time=`date +%s`
((deploy_time= end_time - start_time))

if [[ $cache_flag != $CACHED_NOW  ]] ; then
	echo "time to deploy $size_image is $deploy_time s"  
fi

umount $mounting_point_hidden 
sleep 3