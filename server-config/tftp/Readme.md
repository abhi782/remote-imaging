#   TFTP Configuration files
Suggested to use tftpd-hpa as the tftp server package. tftpd-hpa is the configuration file tftpd, having following options :
* TFTP_USERNAME : Specify the username which tftpd will run as; the default is "tftp"
* TFTP_DIRECTORY : A string denoting the absolute path of tftp directory accessible through tftp server. Here, "/tftpboot" directory is accessible from tftp server.
* TFTP_ADDRESS : Port through which TFTP server listens.
* TFTP_OPTIONS: Other server specific options. Here, I have used -s option which is ecurity option which only allows one diretcory to be shared. -c option enables the clients  to create files
  on the tftp server.
*  RUN_DAEMON : It makes the tftpd-hpa start as daemon at the startup of the system, if given "yes". <br>
File is located at "/etc/default/tftpd-hpa".

# What does tftp server contains
It should contain the necessary grub core image, modules and tiny OS bootable  image. This is what tftpboot/boot directory contains, it conatins following folders:
*  auto_os : tiny Os which on boot, executes script deploy_log.sh and display a menu to select images automatically.
*  manual_os:  tiny OS image which on boot spawns a shell, instead of executing the script. This can be used for debugging purpose of image deployment.
* grub : It contains network bootable grub  core image and grub modules for i386-pc platform.Refer here to know how to build and grub to include modules in thois location for a different platform and configuration.
* ipxe : It conains ipxe image "ipxe.lkrn" and its intrd file "boot_hdd.ipxe". Both are loaded form tftp server  when network grub needs to boot from local hard disk. IPXE is  open source network boot firmware.
    It  gives functionality to boot from hard disk through command called "sanboot'. For more information refer [this](http://ipxe.org/cmd/sanboot).<br>

The /tftpboot directory is summarised in the directory_structure file.
